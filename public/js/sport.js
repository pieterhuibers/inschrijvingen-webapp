function setupSport()
{
		$("#jaren-roeiervaring").hide();
		$("#andere-sport-naam").hide();
		$("#welke-blessures").hide();
		$("#medisch-informatie").hide();
		$("#allergie-informatie").hide();
		setSportListeners();
}

function setSportListeners()
{
	$("#roeiervaring-nee").click(function()
	{
		$("#jaren-roeiervaring").hide(400);
	});
	$("#roeiervaring-ja").click(function()
	{
		$("#jaren-roeiervaring").show(400);
	});

	$("#andere-sport-nee").click(function()
	{
		$("#andere-sport-naam").hide(400);
	});
	$("#andere-sport-ja").click(function()
	{
		$("#andere-sport-naam").show(400);
	});

	$("#blessures-nee").click(function()
	{
		$("#welke-blessures").hide(400);
	});
	$("#blessures-ja").click(function()
	{
		$("#welke-blessures").show(400);
	});

	$("#medisch-nee").click(function()
	{
		$("#medisch-informatie").hide(400);
	});
	$("#medisch-ja").click(function()
	{
		$("#medisch-informatie").show(400);
	});

	$("#allergie-nee").click(function()
	{
		$("#allergie-informatie").hide(400);
	});
	$("#allergie-ja").click(function()
	{
		$("#allergie-informatie").show(400);
	});

	$("#sport-volgende").click(function()
	{
		activateTab("triton");
	});
}
