function setupTriton()
{
	setTritonListeners();
}

function setTritonListeners()
{
	$("#inleveren").click(function()
	{
		//alert("Weet je zeker dat je al je gegevens correct hebt ingevuld?");
	});

	$('#inschrijf-formulier').submit(function()
	{
		var parameters = getParameters();
		for(var id in parameters)
		{
			parameter = parameters[id];
			$('[name="'+parameter.name+'"]').remove();
			$(this).append('<input type="hidden" name="'+parameter.name+'" value="'+parameter.value+'" /> ');
		}
		return true;
	});

	$("button[type='button']").click(function()
		{
			var length = this.id.length;
			var name = this.id.substring(0,length-2);
			var id = parseInt(this.id.substring(length-1));
			setButtonGreen(name,id);
		});
}

function getParameters()
{
	var parameters = [];
	var allNames = ["hard-roeien",
									"commissies",
									"vrienden",
									"conditie",
									"feesten",
									"hechte-ploeg",
									"sturen"];
	for(var nameIndex=0; nameIndex < allNames.length; nameIndex++)
	{
		name = allNames[nameIndex];
		priority = "";
		for(var buttonIndex=1; buttonIndex <= 3; buttonIndex++)
		{
			buttonID = "#"+name+"-"+buttonIndex;
			button = $(buttonID)[0];
			if(button.className.search("btn-success")>-1)
			{
				priority = ""+buttonIndex;
			}
		}
		parameter = {name: name, value: priority};
		parameters = parameters.concat(parameter);
	}
	return parameters;
}

function setButtonGreen(name,id)
{
	var allIDs = [1,2,3];
	var allNames = ["hard-roeien",
									"commissies",
									"vrienden",
									"conditie",
									"feesten",
									"hechte-ploeg",
									"sturen"];
	allIDs.splice(allIDs.indexOf(id),1);
	allNames.splice(allNames.indexOf(name),1);

	buttonName = "#"+name+"-"+id;
	$(buttonName).attr("class","btn btn-success btn-lg");

	var len = allIDs.length;
	for (var i=0; i<len; ++i)
	{
		buttonName = "#"+name+"-"+allIDs[i];
		$(buttonName).attr("class","btn btn-default btn-lg");
	}

	len = allNames.length;
	for (i=0; i<len; ++i)
	{
		buttonName = "#"+allNames[i]+"-"+id;
		$(buttonName).attr("class","btn btn-default btn-lg");
	}
}
