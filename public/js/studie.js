function setupStudie()
{
		$("#andere-instelling").hide();
		$("#andere-vereniging").hide();
		$("#introductie-vereniging").hide();
		$("#functies-vereniging").hide();
		setStudieListeners();
}

function setStudieListeners()
{
	$("#instelling-uu").click(function()
	{
		$("#andere-instelling").hide(400);
	});
	$("#instelling-hu").click(function()
	{
		$("#andere-instelling").hide(400);
	});
	$("#instelling-anders").click(function()
	{
		$("#andere-instelling").show(400);
	});

	$("#vereniging-nee").click(function()
	{
		$("#andere-vereniging").hide(400);
	});
	$("#vereniging-ja").click(function()
	{
		$("#andere-vereniging").show(400);
	});

	$("#functies-nee").click(function()
	{
		$("#functies-vereniging").hide(400);
	});
	$("#functies-ja").click(function()
	{
		$("#functies-vereniging").show(400);
	});

	$("#studie-volgende").click(function()
	{
		activateTab("sport");
	});
}
