#!/bin/bash
cd -- "$(dirname "$BASH_SOURCE")"
#kill running instance of mongodb if it already exists to make sure we are not writing to an old database
PID=`ps aux | grep -v "grep" | grep "mongo" |awk '{print$2}'`
if [ ! -z "${PID}" ]
then
	echo "killing running mongod instance with PID ${PID}"
	kill ${PID}
	sleep 3
else
	echo "no instance of mongodb running yet"
fi
mongod --dbpath 'database/inschrijvingen' &
# wait until the database daemon has started
sleep 5
node app.js
