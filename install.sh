#!/bin/bash

echo 'Installing mongodb'
curl "https://fastdl.mongodb.org/osx/mongodb-osx-ssl-x86_64-3.2.8.tgz" -o "mongodb.tgz"
tar xvzf mongodb.tgz
echo 'Done!'

echo 'Installing node depencies'
npm install
echo 'Done!'

echo 'Creating desktop links'
cp startServer.sh startServer.command
cp ipAddress.sh ipAddress.command
cd ~/Desktop
ln -s inschrijvingen-webapp/startServer.command startServer.command
ln -s inschrijvingen-webapp/ipAddress.command ipAddress.command
echo 'Done!'
