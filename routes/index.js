var express = require('express');
var router = express.Router();
var parser = require("../lib/userParser");
var writer = require("../lib/userWriter");
var csvWriter = require("../lib/csvWriter");
var dbLib = require("../lib/db");
var csvFile = "csv/inschrijvingen";
var util = require('../lib/util');

/* GET home page. */
router.get('/', function(req, res) {
  res.render("home");
});

router.get("/inschrijven", function(req,res)
{
	res.render("inschrijven");
});

router.get("/success", function(req,res)
{
	res.render("success");
});

router.post("/submit", function(req,res)
{
	var user = parser.parseUser(req.body);
	writer.writeUserToMongoDB(user);
	console.log("Submitted form");
  res.redirect("/success");
});

router.get("/overzicht", function(req,res)
{
	dbLib.findAllUsers(function(error, users) {
		if(!error && users!==undefined) {
			console.log("Found "+users.length+" users, rendering list");
			var ipAddress = util.getIpAddress();
			var port = require("../app").getPort();
			res.render("overzicht", {inschrijvingen: users, ipAddress: ipAddress, port: port});
		} else {
			console.log("Could not find any users");
			console.log(error);
		}
	});
});

router.get("/details/:userId", function(req,res) {
	console.log(req.params);
	dbLib.findUserById(req.params.userId, function(error, user) {
		if(!error && user!==undefined) {
			res.render("details", {user: user});
		} else {
			console.log("Could not find user with id "+id);
			console.log(error);
		}
	});
});

router.get("/delete/:userId", function(req,res) {
	console.log(req.params);
	dbLib.deleteUser(req.params.userId, function(error) {
		if(!error) {
			res.redirect("/overzicht");
		} else {
			console.log("Could not find user with id "+id);
			console.log(error);
		}
	});
});

router.post("/edit", function(req,res) {
	console.log(req.body);
	dbLib.updateUserWithId(req.body.id, req.body.opmerkingen, function(error, user) {
		if(!error && user!==undefined) {
			res.redirect("/overzicht");
		} else {
			console.log("Could not find user with id "+id);
			console.log(error);
		}
	});
});

router.get("/csv", function(req,res)
{
	console.log("Downloading csv file");
	var inschrijvingen = dbLib.findAllUsers(function(error, users)
	{
		if(!error && users!==undefined)
		{
			try
			{
				csvWriter.createCSVFile(users,csvFile+"_"+util.getTimestamp()+".csv");
				res.redirect("/overzicht");
			}
			catch(err)
			{
				res.send("Could not write to csv file, maybe it is opened by another program");
			}
		}
		else
		{
			console.log("Could not find any users");
			console.log(error);
			res.redirect("/overzicht");
		}
	});
});

router.get("/csv-vandaag", function(req,res)
{
	var vandaag = new Date();
	vandaag.setHours(0,0,0,0);
	var dateString = vandaag.getDate() + "-" + (vandaag.getMonth()+1) + "-" + vandaag.getFullYear();
	console.log("Downloading csv file for date " + dateString);
	var inschrijvingen = dbLib.findAllUsersOnDate(vandaag,function(error, users)
	{
		if(!error && users!==undefined)
		{
			try
			{
				csvWriter.createCSVFile(users,csvFile+"_"+util.getTimestamp()+".csv");
				res.redirect("/overzicht");
			}
			catch(err)
			{
				res.send("Could not write to csv file, maybe it is opened by another program");
			}
		}
		else
		{
			console.log("Could not find any users");
			console.log(error);
			res.redirect("/overzicht");
		}
	});
});

module.exports = router;
