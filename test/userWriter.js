var writer = require("../lib/userWriter");
var expect = require("chai").expect;
var dbLib = require("../lib/db");
var fs = require("fs");

var excelFileName = "excel\\test.xlsx";
var databaseName = "inschrijvingenDatabase";
var userCollectionName = "testInschrijvingen";
var db;

describe("userWriter", function()
{
  describe("#writeUserToExcel()", function()
	{


		it("it should create a new excel file if one does not yet exist", function()
		{
		});

		it("Should create a lock file when writing a user to the excel file", function()
		{
			//TODO actually test for existence of lock file
		});

		it("it should throw an error when it cannot write to the excel file", function()
		{
		});

		it("it should add the user to the excel file", function()
		{
		});

		it("it should add the date and time when the user was added", function()
		{
		});
	});

  describe("#writeUserToMongoDB()", function()
	{
		function connectToDatabase()
		{
			db = dbLib.connect(databaseName,[userCollectionName]);
		}

		function clearDatabase(done)
		{
			dbLib.removeAll(userCollectionName, function()
			{
				done();
			});
		}

		before(connectToDatabase);
		afterEach(clearDatabase);
		

		it("it should throw an error when it can not connect to the specified database", function()
		{
		});

		it("it should add the user to the database", function()
		{
			var	user = generateRandomUser();
			writer.writeUserToMongoDB(user,databaseName,userCollectionName);
			//TODO test if inserted
		});

		it("it should add the date and time when the user was added", function()
		{
		});
	});
});

function populateExcelFileWithData(nrOfRowsToFill)
{
	user = {};
	for(var i=0; i < nrOfRowsToFill; i++)
	{
		user.a = "A"+(i+1);
		user.b = "B"+(i+1);
		user.c = "C"+(i+1);
		user.d = "D"+(i+1);
		user.e = "E"+(i+1);

		writer.writeUserToExcel(user);
	}
}

function generateRandomUser()
{
	var voornamen = ["asd", "def", "ghj", "kl"];
	var achternamen = ["qwe", "rty", "uio", "p"];
	user = {};
	user.voornaam = voornamen[Math.floor(Math.random()*voornamen.length)];
	user.achternaam = achternamen[Math.floor(Math.random()*achternamen.length)];
	return user;
}
