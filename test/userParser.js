var parser = require("../lib/userParser");
var expect = require('chai').expect;

describe('userParser', function()
{
  describe('#parseUser()', function()
	{
		it("should create a json object from the body with exactly the same number as fields as the body", function()
		{
			nrOfFields = 5;
			body = createBodyWithFields(nrOfFields);
			user = parser.parseUser(body);
			expect(Object.keys(user).length).to.equal(5);
		});

		it('should convert the date field to a Date object', function()
		{
			body = createSampleUserRequestBody();
			user = parser.parseUser(body);
			expect(user.geboortedatum).to.be.a("Date");
		});
  });
});

function createBodyWithFields(nrOfFields)
{
	body = {};
	for(i=0; i<nrOfFields; i++)
	{
		body[""+i] = "field";
	}
	return body;
}

function createSampleUserRequestBody()
{
	body = {};
	body =
	{
		voornaam      : "Pieter",
		achternaam    : "Huibers",
		initialen     : "P.F.",
		geboortedatum : "1984-04-17",	//Submitted in this form by http post request
		telefoon      : "06-24385986",
		rekening      : "7912376"
	};

	return body;
}
