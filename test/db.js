var database = require("../lib/db");
var expect = require('chai').expect;

var databaseName = "inschrijvingenDatabase";
var collectionName = "testInschrijvingen";
var db;

describe('db', function()
{
	function connectToDatabase()
	{
		db = database.connect(databaseName,[collectionName]);
		return db;
	}

	function clearDatabase(done)
	{
		database.removeAll(collectionName, function()
		{
			done();
		});
	}


  describe('#connect()', function()
	{
		it("should connect to the MongoDB database with the correct collection(s)", function()
		{
			db = connectToDatabase();
			expect(db).not.to.equal(null);
			expect(db).to.have.property(collectionName);
		});
	});

  describe('#insertUser()', function()
	{
		before(connectToDatabase);
		afterEach(clearDatabase);

		it("should insert a single user in the correct database and collection without error", function(done)
		{
			user = generateRandomUser();
			database.insertUser(user, function(error)
			{
				expect(error).to.equal(false);
				done();
			});
		});

		it("should store all the insertions in the collection", function(done)
		{
			user1 = generateRandomUser();
			user2 = generateRandomUser();
			user3 = generateRandomUser();
			database.insertUser(user1, function(error)
			{
				database.insertUser(user2, function(error)
				{
					database.insertUser(user3, function(error)
					{
						//check to see if 3 users were inserted
						db[collectionName].find("",function(error,results)
						{
							expect(results).to.have.length(3);
							done();
						});
					});
				});
			});
		});
	});

	describe("#removeAll", function()
	{
		it("should remove all previously added users", function(done)
		{
			user1 = generateRandomUser();
			user2 = generateRandomUser();
			user3 = generateRandomUser();
			database.insertUser(user1, function(error)
			{
				database.insertUser(user2, function(error)
				{
					database.insertUser(user3, function(error)
					{
						//check to see if 3 users were inserted
						db[collectionName].find("",function(error,results)
						{
							expect(results).to.have.length(3);
							database.removeAll(collectionName, function(error)
							{
								expect(error).to.equal(false);
								db[collectionName].find("",function(error,results)
								{
									expect(results).to.have.length(0);
									done();
								});
							});
						});
					});
				});
			});
		});
	});
});


function generateRandomUser()
{
	var voornamen = ["asd", "def", "ghj", "kl"];
	var achternamen = ["qwe", "rty", "uio", "p"];
	user = {};
	user.voornaam = voornamen[Math.floor(Math.random()*voornamen.length)];
	user.achternaam = achternamen[Math.floor(Math.random()*achternamen.length)];
	return user;
}
