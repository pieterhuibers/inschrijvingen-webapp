var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var csvWriter = require("./lib/csvWriter");

var routes = require('./routes/index');
var users = require('./routes/users');
var util = require('./lib/util');

var app = express();

var dbLib = require("./lib/db");
var databaseName = "inschrijvingen";
var collectionName = "inschrijvingen";
var csvFile = "csv/inschrijvingen";

var port = 80;
var minutesBetweenBackups = 15;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')))
app.use(logger('dev'));
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;

var server = app.listen(process.env.port || port, function()
{
	dbLib.connect(databaseName,[collectionName]);
	var ipAddress = util.getIpAddress();
	console.log("Started listening on "+ipAddress+":"+port);
	backup();
});

var backup = function()
{
	writeBackup();
	setTimeout(backup, 15*60*1000);
}

function writeBackup()
{
	console.log("Writing backup");
	var inschrijvingen = dbLib.findAllUsers(function(error, users)
	{
		if(!error && users!==undefined)
		{
			try
			{
				csvWriter.createCSVFile(users,csvFile+"_"+util.getTimestamp()+".csv");
				console.log();
				console.log("Backup successful");
			}
			catch(err)
			{
				console.log("Could not write to CSV file, maybe it is opened by another program");
				console.log(err);
			}
		}
		else
		{
			console.log("Could not find any users");
		}
	});
}

module.exports =
{
	getPort : function() {
		return port;
	}
}
