exports.parseUser = parseUser;

function parseUser(body)
{
	user = {};
	var keys = Object.keys(body);
	for(var i=0; i<keys.length; i++)
	{
		questionName = keys[i];
		user[questionName] = body[questionName];
	}

	//Specific parse actions
	//Explicitly parse the date (dd/mm/yyyy) fields
	var dag = body.geboortedatumdag;
	var maand = body.geboortedatummaand;
	var jaar = body.geboortedatumjaar;
	user.geboortedatum	= new Date(jaar,maand-1,dag);
	delete user.geboortedatumdag;
	delete user.geboortedatummaand;
	delete user.geboortedatumjaar;
	user.opmerking = '';
	
	return user;
}
