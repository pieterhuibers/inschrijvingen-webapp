var fs = require('fs');
var eol = require('os').EOL;

module.exports =
{
	createCSVFile: function(users, fileName)
	{
		console.log('Creating csv file '+fileName);
		if(users === undefined || users.length === 0)
			return;
		//Write header
		var first = users[0];
		var header_line = '';
		for (key of order) {
			header_line = header_line + key + ',';
		}
		header_line = header_line.slice(0,-1) + eol;
		fs.appendFile(fileName, header_line, function (err) {
			if (err) {
				console.log('Error writing to CSV file ' + err);
				return;
			}
		});
		for (user of users) {
			user_line = '';
			for (key of order) {
				value = user[key];
				if (value == null) {
					value = '';
				} else {
					value = value.toString().replace(/,|\n|\r/g,' ');
				}
				user_line = user_line + value + ',';
			}
			user_line = user_line.slice(0,-1) + eol;
			user_line = user_line.replace('\'','').replace('\"','');
			fs.appendFile(fileName, user_line, function (err) {
				if (err) {
					console.log('Error writing to CSV file ' + err);
					return;
				}
			});
		}
		console.log('Successfully written CSV file '+fileName);
	}
};

var order = [
	'initialen',
	'voornaam',
	'tussenvoegsel',
	'achternaam',
	'geboortedatum',
	'geslacht',
	'email',
	'telefoon',
	'adres-kamer',
	'postcode-kamer',
	'plaats-kamer',
	'adres-ouders',
	'postcode-ouders',
	'plaats-ouders',
	'telefoon-ouders',
	'email-ouders',
	'andere-vereniging',
	'functies-vereniging',
	'studentnummer',
	'instelling',
	'andere-instelling',
	'faculteit',
	'studie',
	'start-studie',
	'rekening-iban',
	'rekening-bic',
	'shirtmaat',
	'allergie-informatie',
	'vega',
	'lengte',
	'gewicht',
	'sportervaring',
	'trainen-per-week-nu',
	'sportprestaties',
	'roeiervaring',
	'jaren-roeiervaring',
	'motivatie',
	'trainen',
	'welke-blessures',
	'medisch',
	'medisch-informatie',
	'roken',
	'inschrijfavond',
	'barbecue',
	'opmerking'
];
