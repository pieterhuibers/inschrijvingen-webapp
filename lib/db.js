var mongojs = require("mongojs");
var databaseName;
var userCollectionName;
var db;

module.exports =
{
	connect : function(database, collection)
	{
		//connect to mongoDB database
		databaseName = database;
		userCollectionName = collection;
		db = mongojs(database, collection);
		return db;
	},

	findAllUsers : function(callback)
	{
		//retrieves all users
		db[userCollectionName].find().sort({"inschrijfdatum":1},function(err, users) 
		{
  			if( err || !users)
				{
					console.log("No users found");
					callback(err,null);
				}
  			else
		 		{
					callback(false,users);
				}
		});
	},

	findAllUsersOnDate : function(date, callback)
	{
		//retrieves all users
		db[userCollectionName].find({"inschrijfdatum" : { $gte : date }}).sort({"inschrijfdatum":1},function(err, users) 
		{
  			if( err || !users)
				{
					console.log("No users found");
					callback(err,null);
				}
  			else
		 		{
					callback(false,users);
				}
		});
	},

	findUserById : function(id, callback) {
		//retrieves all users
		var objectId = new mongojs.ObjectID(id);
		db[userCollectionName].findOne({"_id" : objectId}, function(err, user) {
  			if( err || !user) {
					console.log("No user found with id "+id);
					callback(err,null);
				} else {
					callback(false,user);
				}
		});
	},

	updateUserWithId : function(id, note, callback) {
		//retrieves all users
		var objectId = new mongojs.ObjectID(id);
		db[userCollectionName].update({"_id" : objectId}, {$set:{"opmerking":note}}, function(err, user) {
  			if( err || !user) {
					console.log("No user found with id "+id);
					callback(err,null);
				} else {
					callback(false,user);
				}
		});
	},

	//TODO remove collection parameter
	removeAll : function(collection, callback)
	{
		db[collection].remove(function(error,document)
		{
			if(error)
				callback(true);
			else
				callback(false);
		});
	},

	deleteUser : function(id, callback) {
		var objectId = new mongojs.ObjectID(id);
		db[userCollectionName].remove({"_id" : objectId}, function(err, user) {
  			if( err || !user) {
					console.log("No user found with id "+id);
					callback(true);
				} else {
					callback(false);
				}
		});
	},

	insertUser : function(user, callback)
	{
		db[userCollectionName].save(user, function(err, saved) 
		{
  		if( err || !saved )
			{
				callback(true);
			}
  		else
			{
				callback(false);
			}
		});
	}
};
