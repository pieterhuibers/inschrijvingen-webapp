var fs = require("fs");
var lock = require("lockfile");
var dbLib = require("../lib/db");

module.exports =
{
	writeUserToMongoDB: function(user)
	{
		//get the current date time and save that as well
		var date = new Date();
		user.inschrijfdatum = date;
		dbLib.insertUser(user, function(error)
		{
			if(!error)
				console.log("Added user "+user.voornaam+" "+user.achternaam+" to database");
			else
				console.log("Error writing user "+user.voornaam+" "+user.achternaam+" to database");
		});
	}
};
