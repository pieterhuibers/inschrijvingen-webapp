module.exports =
{
	zeroFill : function(number, width) {
			width -= number.toString().length;
			if (width > 0) {
				return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
			}
			return number + "";
	},

	getTimestamp : function() {
		var d = new Date();
		var year = d.getFullYear();
		var month = this.zeroFill(d.getMonth()+1,2);
		var day = this.zeroFill(d.getDate(),2);
		var hour = this.zeroFill(d.getHours(),2);
		var minute = this.zeroFill(d.getMinutes(),2);
		var second = this.zeroFill(d.getSeconds(),2);
		return year+"-"+month+"-"+day+"_"+hour+"-"+minute+"-"+second;
	}, 

	getIpAddress : function() {
		var os = require('os');
		var ifaces = os.networkInterfaces();
		var ipAddress;
		Object.keys(ifaces).forEach(function (ifname) {
			var alias = 0;

			ifaces[ifname].forEach(function (iface) {
				if ('IPv4' !== iface.family || iface.internal !== false) {
					return;
				}

				if (alias >= 1) {
					return;
				} else {
					ipAddress = iface.address;
				}
				++alias;
			});
		})
		return ipAddress;
	}
}
